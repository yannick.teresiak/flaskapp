# Changelog

All notable changes to this project will be documented in this file.

## [unreleased]

### Bug Fixes

- Fix: update S201 to S200 to match the actual type

- Fix: add werkzeug version requirement

- Fix: add bagmax2 attribute to new plane

- Fix: add missing plane attributes


### Documentation

- Docs: add CHANGELOG

- Docs: update CHANGELOG


### Miscellaneous Tasks

- Chore: add git-cliff configuration

- Chore: CHANGELOG


### Other

- Dev: add new plane to fleet

- Dev: deprecated cobertura report

- Dev: add TO/LD data for new plane


## [3.1.2] - 2021-09-25

### Other

- Update prepflight.html
- Update report.html
- Update prepflight.html
- Merge branch 'cdb' into 'master'

pax to cdb for pax0

See merge request yannick.teresiak/flaskapp!10
- Update fleet.yaml bew and arm["bew"] for S201 based on actual weight balance sheet

- Merge branch 'sonaca_delivery' into 'master'

Update fleet.yaml bew and arm["bew"] for S201 based on actual weight balance sheet

See merge request yannick.teresiak/flaskapp!11

## [3.1] - 2021-06-14

### Other

- Sonaca

- Merge branch 'sonaca' into 'master'

Sonaca

Closes #1 and #2

See merge request yannick.teresiak/flaskapp!9

## [3.0] - 2020-12-17

### Other

- Add LICENSE
- Added ref to git repo

- Fuel in litres. Flight duration added to report.

- Merge branch '5-fuel-aux-fuel-input-in-litres' into 'master'

Fuel in litres. Flight duration added to report.

Closes #5

See merge request yannick.teresiak/flaskapp!7
- V3 aerogest online

- Merge branch '6-aerogest-online' into 'master'

v3 aerogest online

Closes #6

See merge request yannick.teresiak/flaskapp!8

## [2.4] - 2020-10-03

### Other

- App as a package

- Fixes

- Merge branch '4-static-files-in-package'


## [2.1] - 2020-09-28

### Bug Fixes

- Fixed cloudbuild.yaml and gitignore


### Other

- Initial commit

- Added static dir + css and favicon

- Added static. Added specific css for dataframes. Added the planes module.

- Housekeeping in planes.py

- Minor fixes

- Working !

- Working version

- Functions in js, form.css added, validation OK

- Refactored prediction in PlanePerf. Working well.

- Printable report

- SelectField fail to valid. Fix in  JQuery. Working

- Working and beautiful

- Before refactorization

- Refactored as a package. Working.

- On GCP !

- New navbar. All good.

- Rid od db and flask-login. WOW !

- Responsive OK. WOW !

- Aux fuel gauge. Zp and Zd. Working well.

- Flex form OK. WOW!

- Fixed reset of plane + coerce aux gauge to float

- Forgot prepflight.js

- Fleet added as cards. WOW !

- Fixed title in fleet.html

- Add README.md
- Delete Dockerfile
- Docstrings

- Fixed fuel flow rate. Pretty select box. Endurance

- Fixed call signs. Rounded down endurance to multiple of 5mn. Tested OK

- Commented it out printouts

- Fixed fuel flow rate and form section wrapper.

- Fixed report missing data and fixed A4 display

- Fixed last quarter in logbook

- Refactored main.py for docker

- Docker-hub-ized

- Running on Cloud Run w/ recipe

- Add README.md
- Add .gitlab-ci.yml
- Distroless + CI/CD setup for GitLab

- Merge branch 'master' of gitlab.com:yannick.teresiak/flaskapp

Added .gitlab-ci.yml on GitLab repo.

- Added .env.prod variables to run step cloudbuild.yaml

- Added Prod env to Dockerfile

- Env vars as gunicorn args in entrypoint

- Fixed APP_FOLDER value in .env.prod

- Removed messages display from fleet.html

- Externalized POH data to csv files in data
Externalized fleet data to planes.yaml
Added PyYAML to requirements
Authored programs.

- Added path handling for data files

- Added CI/CD tests w/ unittest and pytest

- Fixed pipeline stages order

- Fixed call to python in test stage

- Fixed gitlab-ci.yaml

- Fixed gitlab-ci.yaml

- Fixed gitlab-ci.yaml

- Fixed gitlab-ci.yaml with apk

- Fixed gitlab-ci.yaml with apk

- Fixed gitlab-ci.yaml

- CI/CD Master only. Traffic to latest cloud run service revision

- Little fix in gcloud command

- First try at gitlab-ci-token to push to docker hub

- Second try at gitlab-ci-token to push to docker hub

- Third try at gitlab-ci-token to push to docker hub

- CI test

- Fourth try at gitlab-ci-token to push to docker hub

- CI test

- Fifth try at gitlab-ci-token to push to docker hub

- Sixth try at gitlab-ci-token to push to docker hub

- 7th try at gitlab-ci-token to push to docker hub

- 8th try at gitlab-ci-token to push to docker hub

- 9th try at gitlab-ci-token to push to docker hub

- 10th try at gitlab-ci-token to push to docker hub

- 11th try at gitlab-ci-token to push to docker hub

- Added fix DOCKER_HOST=tcp://localhost:2375

- Added fix DOCKER_HOST: tcp://localhost:2375

- Using stable-dind

- Using stable-dind in gcloud stage

- Added tcp DOCKER_HOST to use the DinD service

- Tcp DOCKER_HOST docker:2375 to use the DinD service

- Added JUnit report

- Fixed JUnit report

- Linting

- Updated file tree + pytest in vscode

- Updated file tree + pytest in vscode

- Fixed config.py + added back __init__ in tests

- Black format + CI/CD DAG

- Added coverage report

- Added pytest-cov to requirements

- Updates to dockerignore and gitignore - rm of cached egg-info

- Linting completed in planes.py

- - Added black formatter config in pyproject.toml
- Added .flake8
- Some changes to README
- Flask-Session to requirements.txt
- Configurations for DEV, PROD and TESTING
- Server-side cookies for Flask sessions
- Added the plot_balance test

main.py was refactored to get rid of global variables.
Flask session was added to take full advantage of session variables.
BUT: there are problems:
- The planes FlightLog class is not serializable so far. I had
  to serialize the logbook attribute by converting the dataframe
  to HTML. Uggly.
  I also had to add attributes flightstats and quarterstats to the
  class to avoid converting the dataframes every time. NOT GOOD.

- Merge branch 'dev' into 'master'

Dev v2.0

See merge request yannick.teresiak/flaskapp!1
- Added coverage report + badge

- Proper README

- New tests + fix FlightLog.heures for empty series

- Added timeout to failed login test. Ref #1

- Merge branch '1-test-lacking-timeout-2'

- Package OK

- Merge branch '2-reorganize-the-package-file-tree-following-best-practices'

- Added PYTHONPATH to tests in gitlab-ci

- Fixed test

- Fixed reports paths

- Fixed STATIC_FOLDER

- V2.1 tagged


<!-- generated by git-cliff -->
